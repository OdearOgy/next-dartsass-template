import type { NextPage } from 'next'

import {home, title} from '../styles/scss/pages/home.module.scss';

const Home: NextPage = () => {
  return (
    <div className={home}>
      <h1 className={title}>hello, world!</h1>
    </div>
  )
}

export default Home
